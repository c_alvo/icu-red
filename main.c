/*
Basically just handles the "client" and "server" threads.
Also, contains the thread which periodically reports uptime to wcpkneel.
*/

#include <pthread.h>
#include "system_tools.h"
#include "tcp_tools.h"
#include "client.h"
#include "server.h"

void heartbeat() { //Not to be confused with the part of SSL that heartworm exploited, although it bears certain similarities
	char params[256];
	char *format = "?i=%s&uptime=60";
	sprintf(params, format, UNAME);
	while (1) {
    int sockfd = clientConnect(PILOT);
    if (httpget(sockfd, params, 0) < 1) {
      printf("uptime report failed\n");
    }
    /*char buf[1024];
    recv(sockfd, buf, 1024, 0);
    printf("%s\n", buf);*/
    close(sockfd); //Since we are only doing this once a minute, I suspect it is better to close the connection and start a new one each time.
    sleep(60);
  }
}

void *clients_thread() {
	printf("Client loop running\n");
	scanloop();
	return NULL;
}

void *server_thread() {
	char wapname[256];
	getWap(wapname, 256);
	printf("(%s) server loop running from WAP: %s\n", UNAME, wapname);
	server();
	printf("The server encountered an issue\n");
	return NULL;
}

void main(){

	pthread_t clients_thread_id;
	pthread_t server_thread_id;

	pthread_create(&clients_thread_id, NULL, clients_thread, NULL);
	pthread_create(&server_thread_id, NULL, server_thread, NULL);

	heartbeat();
	return;
}
