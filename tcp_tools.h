#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>

#define PORT 28900
#define PILOT "10.115.20.250"
#define UNAME "tkirby" //The spelling of this had me confused why the scoreboard wasn't updating for a while :/
#define SCAN_DELAY 25  //In hundredths of a second

// Initializes a basic socket
int initSock() {
	int sockfd;
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	if (sockfd == -1) {
		printf("Cannot create socket.\n");
		return -1;
	}
	return sockfd;
}

// Configure a timeout in hundredths of a second.
void setTimeout(int sockfd, int timeout) {
	struct timeval timev; //Credits to the class powerpoint for code that creates a timeout.
	timev.tv_sec = 0;
	timev.tv_usec = timeout * 10000;
	setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO|SO_SNDTIMEO|SO_REUSEADDR|SO_REUSEPORT, &timev, sizeof(timev));
}

// Initializes a sockaddr_in struct with some good defaults.
struct sockaddr_in initSockaddr() {
	struct sockaddr_in server_addr;
	memset(&server_addr, '0', sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(PORT);
	return server_addr;
}

// Creates a TCP client connection
int clientConnect(char *ipv4) {
	int sockfd = initSock();
	setTimeout(sockfd, SCAN_DELAY); //One tenth a second of timeout.
	struct sockaddr_in server_addr = initSockaddr();

	int init = inet_pton(AF_INET, ipv4, &server_addr.sin_addr);
	if (init < 1) {
		printf("Inet_pton failed with code %d.\n", init);
		return -1;
	}
	if (connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1) {
		//printf("Cannot connect to %s.\n", ipv4);
		return -1;
	}

	return sockfd;
}

// Send an HTTP GET request pilot.westmont.edu
int httpget(int sockfd, char *params, int verbose) {
	char request[1024];
	char *format = "GET /%s HTTP/1.0\r\n\r\n";
								 //"Host: pilot.westmont.edu:28900\r\n\r\n";
	sprintf(request, format, params);
	if (verbose) {
		printf("%s\n", request);
	}
	return send(sockfd, request, strlen(request), 0);
}

// TCP send() encapsulation
int sendout(char *msg, int sockfd) {
	if(send(sockfd,msg,strlen(msg),0) < 1){
		printf("Failed to send.\n");
		return -1;
	}
	return 1;
}

// TCP recv() encapsulation
int receive(char *buffer, int sockfd) {
	if(recv(sockfd, buffer, 256, 0) < 1) {
		printf("Failed to receive.\n");
		return -1;
	}
	return 1;
}

int client(char *ipv4); //Forward declaration for the benefit of delayed_attack.

void *delayed_attack(void *arg) { //Attack a host after 15 minutes elapse, either because we found them or they found us.
	sleep(900);
	char *ipv4 = (char *)arg;
	client(ipv4);
}

// Server-side TCP intialization
int serverStart(int sockfd, struct sockaddr_in server_addr){
	int optval = 1;
	//server_addr->sin_addr.s_addr = htonl(INADDR_ANY);
	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEPORT, (const void *)&optval, sizeof(int)) < 0) {
    printf("setsockopt failed\n");
    return -1;
  }
	if (bind(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1) {
		printf("Failed to bind to socket\n");
		return -1;
	}
	return 1;
}

// returns client file descriptor, and pointer clientIPv4 points to the clients IPv4 address.
int getClient(int sockfd, char *clientIPv4){
	int clientfd, clientlen;
	struct sockaddr_in client_addr;
	if (listen(sockfd, 1) < 0) {
		printf("Failed to listen\n");
    return -1;
	}
	clientlen = sizeof(client_addr);
	if ((clientfd = accept(sockfd, (struct sockaddr *)&client_addr, &clientlen)) == -1) {
  	printf("Failed to accept client connection\n");
  	return -1;
  }
	clientIPv4 = inet_ntoa(client_addr.sin_addr);
	printf("Incoming connection from %s\n", clientIPv4);
	pthread_t thread_id;
	pthread_create(&thread_id, NULL, delayed_attack, clientIPv4);
	return clientfd;
}
