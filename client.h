/*
Client code
Searches for other teams and reports to pilot.westmont.edu when it finds victims
*/

#include <pthread.h>

int client(char *ipv4) {
	int sockfd;
	char buffer[256];
	char *victimID, *victimAP;

	if ((sockfd = clientConnect(ipv4)) < 1){ //Attempt to connect to the specified host.
		return -1;
	}
	printf("Connected to %s\n", ipv4);

	if (send(sockfd, "Who are you?\n", 13, 0) < 1) {
		return -1;
	}
	if (recv(sockfd, buffer, 256, 0) < 1) {
		return -1;
	}

	char *split = strchr(buffer, ' '); //Process the buffer and figure out the victimID and victimAP
	if ( split != NULL) {
		*split = '\0';
	}
	else {
		return -1;
	}

	victimID = buffer;
	victimAP = split + 1;

	printf("Found %s at %s\n",victimID, victimAP);

	if (victimID == "tkirby" || victimID == "bethomas" || victimID == "calvo" || victimID == "jawilkens") {
		printf("Averting friendly fire...");
		close(sockfd);
		return -1;
	}

	char params[256];
  char *format = "?i=%s&u=%s&where=%s";
  sprintf(params, format, UNAME, victimID, victimAP);
	int pilotfd = clientConnect(PILOT);
	if (httpget(pilotfd, params, 1) > 0) {
		printf("Reported to server\n");
	}
	close(pilotfd);
	close(sockfd);

	pthread_t thread_id;
	pthread_create(&thread_id, NULL, delayed_attack, ipv4);
}

/*
Scan the IP range:
	10.90.{1-20,64-75,128-138}.1-253
	10.127{0-10,64-75,128-138,192-202}.1-253
*/
int scanloop() { //Note to self: find an elegant way to make this less repetitive, both in terms of the looping structure and the sscanf() and client() calls.
	char host[32];
	int i;
	int k;
	while (1) {
		for (k=1; k<=253; k++) {
			for (i=1; i<=20; i++) {
				sprintf(host, "10.90.%d.%d", i, k);
				client(host);
			} for (i=64; i<=75; i++) {
				sprintf(host, "10.90.%d.%d", i, k);
				sprintf(host, "10.124.%d.%d", i, k);
				client(host);
			} for (i=128; i<=138; i++) {
				sprintf(host, "10.90.%d.%d", i, k);
				sprintf(host, "10.124.%d.%d", i, k);
				client(host);
			} for (i=0; i<=10; i++) {
				sprintf(host, "10.124.%d.%d", i, k);
				client(host);
			} for (i=192; i<=202; i++) {
				sprintf(host, "10.124.%d.%d", i, k);
				client(host);
			}
		}
	}
}
