/*
Server code
Accepts pings of "Who are you?" every 15 minutes and gives the correct response
*/

int server () {
	int serverfd, clientfd;
	char buffer[256];
	char wap[128];
	char clientIPv4[128];

	// Setup sockadder_in struct
	struct sockaddr_in server_addr;
	memset(&server_addr, '0', sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(PORT); // set port to 28900
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);

	//Create server file descriptor
	serverfd = initSock();
	if (serverfd < 0){
		return -1;
	}

	//Wrapped sockopt and bind functions to start server
	if (serverStart(serverfd, server_addr) == -1){
		return -1;

	}
	//printf("Server running\n");

	//Infinate loop which opens connections with attackers and then stalls 15 min
	while (1){
		memset(clientIPv4, 0, strlen(clientIPv4));
		//Wrapped listen and accept functions/ store attacker's IPv4 address
		clientfd = getClient(serverfd, clientIPv4);
		printf("\nhoooray\n");
		if (clientfd == -1){
			printf("\nRetrying listen/accept\n");
			continue;//Restart loop for faulty connection
		}

		//Clear buffer to recieve attack message
		memset(buffer,0, strlen(buffer));
		receive(buffer, clientfd);

		printf("recieved: %s\n", buffer);

		//Proccess attacker message and send
		if (!strcmp("Who are you?\n", buffer)){
			memset(buffer,0, strlen(buffer));
			getWap(wap,128);
			sprintf(buffer, "%s %s\n", UNAME, wap);// create "username accesspoint\n" string
			sendout(buffer, clientfd);
			memset(wap, 0, 128);
			printf("\n%s made a successful attack!\n", clientIPv4);
			close(clientfd);
			printf("\n......Initiating 15 minute cooldown......\n");
			sleep(900); // wait 15 minutes before accepting another attack
		} else {
			close(clientfd);
		}
	}

}
