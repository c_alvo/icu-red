/*
System utility functions
*/

#include <stdio.h>
#include <stdlib.h>

#define COMMAND "cat bssids | grep `iwconfig 2>&1 | grep Access | awk '{print $6}' | tr -d : | tr '[:upper:]' '[:lower:]'` | awk '{print $1}'" //The bash equivalent of a run-on sentence.
#define COMMAND_S "cat bssids | grep `iwconfig 2>&1 | grep Access | awk '{print $6}' | tr -d : | tr '[:upper:]' '[:lower:]'` | awk '{print $1}'"

// Gets the name of the access point we are connected to.
void getWap(char *buffer, int size) {
  FILE *mycmd = popen(COMMAND, "r"); //Credits to John Rodkey for detailing the popen call.
  fgets(buffer, size, mycmd);
  pclose(mycmd);
}

void sanitizeWap(char *buffer, int size) {

}

void changeIP() {
  return;
}
